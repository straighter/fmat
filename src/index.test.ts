import fmat from "./index";

it("should return empty", () => {
  expect(fmat()).toBe("");
});

it("should be an identity function", () => {
  expect(fmat("hello")).toBe("hello");
});

it("should execute the format", () => {
  expect(fmat("hello", { format: [2, 2] })).toBe("he ll o");
});

it("should correctly format the string with a custom delimiter", () => {
  expect(fmat("hello", { format: [2, 2], delimiter: " " })).toBe("he ll o");

  expect(fmat("hello", { format: [1, 1, 2], delimiter: "." })).toBe("h.e.ll.o");

  expect(fmat("hello", { format: [1, 3], delimiter: ".-" })).toBe("h.-ell.-o");

  expect(fmat("hello", { format: [0, 5], delimiter: "-" })).toBe("-hello");
});

it("should keep looping the format until the end of the sentence", () => {
  expect(fmat("covfefe", { format: [1, 2], execution: "loop" })).toBe(
    "c ov f ef e"
  );
});

it("should keep looping the last formatter until the end of the sentence", () => {
  expect(fmat("+3161234567890", { format: [4, 2], execution: "hang" })).toBe(
    "+316 12 34 56 78 90"
  );
});

it("should use a custom delimiter for each format array entry", () => {
  expect(
    fmat("+3161234567890", { format: [{ step: 4, delimiter: "-" }, 2] })
  ).toBe("+316-12 34567890");

  expect(
    fmat("+3161234567890", {
      execution: "hang",
      format: [{ step: 4, delimiter: "-" }, 2]
    })
  ).toBe("+316-12 34 56 78 90");

  expect(
    fmat("+3161234567890", {
      execution: "loop",
      format: [{ step: 4, delimiter: "-" }, 2]
    })
  ).toBe("+316-12 3456-78 90");
});

it("should append the prefix and format the string from there", () => {
  expect(
    fmat("1234567890", { format: [4, 2], execution: "hang", prefix: "+316" })
  ).toBe("+316 12 34 56 78 90");
});
