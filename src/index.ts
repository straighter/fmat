export interface IFmatOptions {
  execution: "loop" | "hang" | "exec";
  delimiter: string;
  format: Array<number | { step: number; delimiter: string }>;
  prefix: string;
}

const DEFAULT_FMAT_OPTIONS: IFmatOptions = {
  delimiter: " ",
  execution: "exec",
  format: [],
  prefix: ""
};

export default function fmat(
  input: string = "",
  fmatOptions: Partial<IFmatOptions> = {}
) {
  const options = { ...DEFAULT_FMAT_OPTIONS, ...fmatOptions };
  let formattedString = input;

  if (Array.isArray(options.format) && options.format.length > 0) {
    const inputReference = `${options.prefix}${input}`;
    let formatLoopIndex = 0;
    let startSubstringIndex = 0;
    formattedString = "";

    while (formatLoopIndex !== options.format.length) {
      const format = options.format[formatLoopIndex];
      let delimiter = options.delimiter;
      let step = 0;

      if (typeof format === "object") {
        if ("step" in format && "delimiter" in format) {
          delimiter = format.delimiter;
          step = format.step;
        }
      } else {
        step = format;
      }

      const endSubstringIndex = startSubstringIndex + step;

      if (endSubstringIndex >= inputReference.length) {
        break;
      }

      formattedString += `${inputReference.substring(
        startSubstringIndex,
        endSubstringIndex
      )}${delimiter}`;

      if (
        formatLoopIndex + 1 === options.format.length &&
        options.execution === "loop"
      ) {
        formatLoopIndex = 0;
      } else if (
        formatLoopIndex + 1 === options.format.length &&
        options.execution === "hang"
      ) {
        formatLoopIndex = formatLoopIndex;
      } else {
        formatLoopIndex++;
      }

      startSubstringIndex += step;
    }

    formattedString += `${inputReference.substring(
      startSubstringIndex,
      inputReference.length
    )}`;
  }

  return formattedString;
}
