[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# fmat

A barebones but feature-rich string formatter

# Usage

## Installation

`$ npm install fmat`


## Usage

Import `fmat` into your project.

```js
import fmat from 'fmat';
```

Use it by calling the function with an input value and options, this will always return a string.

```js
const formattedString = fmat('hello', {}); // -> 'hello'
```

### Options

#### `delimiter: string`

Set the delimiter to insert at every format value index within the input string.

*Default: `" "`*

_Usage_

```js
const formattedString = fmat('hello', { format: [1], delimiter: '!' }); // -> 'h!ello'
```

#### `execution: 'loop' | 'hang' | 'exec'`

Set the behaviour when the string is longer than the available format indexes.

- `loop` - loops the format until the end of the input string.
- `hang` - loops the last format value until the end of the input string.
- `exec` - runs just the format and quits after.

*Default: `'exec'`*

_Usage_

```js
const formattedString = fmat('hello', { format: [1], execution: 'loop' }); // -> 'h e l l o'
const formattedString = fmat('hello', { format: [2, 1], execution: 'hang' }); // -> 'he l l o'
const formattedString = fmat('hello', { format: [2, 1], execution: 'exec' }); // -> 'he l lo'
```

#### `format: Array<number | { step: number, delimiter: string}>`

Set the steps which represent indexes to add a delimiter to. You can specify an array of just a number or an object with the step value and a delimiter for that specific step. These options can be mixed together.

*Default: `[]`*

_Usage_

```js
const formattedString = fmat('hello', { format: [1] }); // -> 'h ello'
const formattedString = fmat('hello', { format: [1, { step: 2, delimiter: '-' }] }); // -> 'h el-lo'
```

#### `prefix: string`

Set a prefix for your input to be included in formatting.

*Default: `""`*

_Usage_

```js
const formattedString = fmat('1234567890', { format: [4, 2], execution: 'hang', prefix: "+316" }); // -> '+316 12 34 56 78 90'
```


# Contributing

Want to contribute? Great! Please branch out from the master version from using a branchname such as `feature/{insert-descriptive-feature-name}` and create a pull request when ready.

Use `npm run build` to build the project.

